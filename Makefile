ISINST_DOCKER     := $(shell which docker)

all: install_deps

## Installation
.PHONY: install_deps
install_deps: install_pdm_deps install_ansible_deps lint_rules install_hooks

.PHONY: install_pdm_deps
install_pdm_deps:
	pdm sync

.PHONY: install_ansible_deps
install_ansible_deps:
	{ \
	set -e ;\
	cd ansible ;\
	chmod og-w . ;\
	mkdir -p .ansible/roles .ansible/collections ;\
	pdm run ansible-galaxy install -r requirements.yml ;\
	echo "\n\n----[ Installed roles and collections ]----\n" ;\
	pdm run ansible-galaxy role list ;\
	pdm run ansible-galaxy collection list -p .ansible/collections ;\
	}

.PHONY: lint_rules
lint_rules:
	cd ansible && bin/lint setup

.PHONY: delete_deps
delete_deps:
	rm -rf ansible/.ansible/ ansible/ansible.log ansible/.lint_rules \
		.venv .python-version .pdm-python .pdm-build .git/hooks/* \
		__pypackages__

.PHONY: renew_deps
renew_deps: delete_deps install_deps

.PHONY: refresh_ansible_deps
refresh_ansible_deps:
	cd ansible && pdm run ansible-galaxy install -r requirements.yml --force --ignore-errors

.PHONY: install_hooks
install_hooks:
	pdm run pre-commit install --install-hooks
