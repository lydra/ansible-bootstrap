# Ansible bootrap

Ce dépôt est un référentiel `Ansible` pour les projets ansible de **[Lydra](https://lydra.fr)**.

Il sert de base à tout nouveau projet Ansible pour gagner du temps.

## Ansible

Ressources : [Site officiel](http://www.ansible.com/), [Ansible Galaxy](https://galaxy.ansible.com/)

### Présentation

Ansible est un outil d’automatisation, il peut configurer des systèmes, déployer des logiciels, et orchestrer les tâches informatiques avancées comme des déploiements continus ou des mises à jour sans coupure de service.

Ansible gère les machines sans agent (`agentless`), il n’y a pas de démon sur les nœuds donc pas de mises à jour client à prévoir et pas de problème du type : "impossible de mettre à jour car l’agent ne tourne pas". C’est donc moins intrusif et moins consommateur en ressources pour les machines. Le seul prérequis sur la machine est d'avoir un environnement Python 2 ou 3.

Ansible est décentralisé : il repose sur les informations d’identification sur le système d’exploitation existant pour contrôler l’accès à des machines distantes via *SSH*. Si nécessaire, il peut facilement se connecter avec Kerberos, LDAP, et d’autres systèmes de gestion d’authentification centralisés.

Les fichiers de configuration décrivent l’état dans lequel le serveur doit-être et non la manière d’amener le serveur à cet état. Ansible est capable par lui-même de savoir comment arriver à cet état. Ansible permet aussi des lancements à blanc pour prévisualiser les modifications qui seront nécessaires pour amener le nœud dans l’état souhaité.

Ansible crée à partir des configurations de simples scripts shell qu’il va envoyer et exécuter sur les nœuds distants à travers une connexion *SSH*.

## Prérequis

### Démarrage Rapide

Installer toutes les dépendances du projet :
```bash
make
```

### L'environnement Python

Pour que tous les contributeurs aient le même environnement de développement, il est important de fixer les dépendances Python et paquets pip.
Pour cela, on utilise désormais [PDM](https://pdm-project.org/latest/).

Il faut d'abord s'assurer d'avoir PDM installé ou à jour. Voir [cet article](https://hackernoon.com/reaching-python-development-nirvana-bb5692adf30c) pour en savoir plus (cf. [doc1](https://pdm-project.org/latest/#recommended-installation-method) :

```bash
pip install --user pdm
```

ou pour mettre à jour

```bash
pdm self update
```

L'option `--user` permet de n'appliquer les changements que pour l'utilisateur, et évite de "casser" l'installation par défaut du système.

On utilise ensuite `mise` (pour en savoir plus sur l'installation de mise, c'est [par ici](#installation-des-outils-tiers-terraform-helm-python-avec-mise).

Pour installer la version de Python que l'on veut, par exemple la 3.8, on utilise la commande suivante :

```bash
mise use -g python@3.8
```

Pour le projet, on initialise l'environnement de travail avec PDM. On a spécifié la version de Python dans le fichier `pyproject.toml`. Pour installer les dépendances définies dans ce fichier, utilisez la commande suivante :

```bash
pdm install
```

Avec PDM, il n'est pas nécessaire d'activer l'environnement virtuel à chaque fois que vous travaillez dans le dossier du projet. PDM utilise la PEP 582 pour gérer les dépendances du projet, ce qui permet d'exécuter les commandes directement sans avoir besoin d'activer un environnement virtuel. Pour exécuter des commandes ou des scripts en utilisant les dépendances du projet, vous pouvez préfixer vos commandes avec `pdm run`.
Par exemple :

```bash
pdm run ara-manage runserver &
```

Pour vérifier la version de Python :

```bash
python --version
Python 3.8.12
```

### Ansible

Ansible est géré par PDM.
Il a été configuré avec la version 6 grâce à la commande :

```bash
pdm install ansible==6.*
```

L'ajout du wildcard (`*`) permet à PDM d'installer la dernière version `6.*` compatible avec les autres dépendances du projet.
Définissez les rôles / collections à utiliser pour le projet dans Le fichier `requirements.yml`. Ils seront installés en local dans le sous-dossier `.ansible/`. On peut retrouver les rôles / collections partagées par la communauté ([ici](https://galaxy.ansible.com/home)).

### ansible-vault

Il est utilisé pour chiffrer et déchiffrer les fichiers sensibles pour qu'ils ne soient pas visibles en clair sur `Github` ou `Gitlab` ([Source](https://www.patricelaurent.net/ansible-vault-cryptage-git)).

Par convention, ces fichiers s’appellent `*.vault.yml`.

Pour s'assurer que ces fichiers soient chiffrés à la volée au moment du commit, ajoutez les éléments suivants à votre fichier `.git/config` :

```bash
[diff "ansible-vault"]
  textconv = .git_crypt_diff
  # Do not cache the vault contents
  cachetextconv = false
[filter "ansible-vault"]
  clean  = .git_crypt_clean
```

Créez votre fichier `.vault_pass` à la racine du projet et contenant le mot de passe du projet.

Il est également nécessaire de créer une variable gitlab-ci nommée `ANSIBLE_VAULT_PASSWORD_FILE`. Pour cela, allez sur la page Gitlab du projet, `settings > CI/CD > Variables > Add variable`. La variable doit être masquée avoir comme flag `Expand variable reference`, comme clé `ANSIBLE_VAULT_PASSWORD_FILE` et comme valeur la même que le mot de passe mis dans le fichier `.vault_pass`.

Une fois cette configuration faite, tous les fichiers contenant "vault" dans leur nom sont automatiquement chiffrés au moment du `commit`. Ainsi, vous ne risquez plus de compromettre des informations sensibles en les partageant en clair sur un dépôt de code.

Les fichiers chiffrés peuvent alors être lus avec `Ansible Vault` :

```bash
ansible-vault edit my_file.vault.yml --vault-password-file=.vault_pass
```

Pour plus de détails, [voir ici](https://www.patricelaurent.net/ansible-vault-cryptage-git/).

Un plugin peut être ajouté dans VSCodium afin de faciliter la gestion des secrets ([dhoeric.ansible-vault](https://github.com/sydro/atom-ansible-vault)).

Nous avons ajouté 3 fichiers à chiffrer dans Ansible-bootstrap : `ansible/group_vars/all/s3.vault.yml`, `ansible/group_vars/remote_backup/borg.vault.yml`, `ansible/group_vars/local_backup/borg.vault.yml`. Il faudra les agrémenter avec les valeurs correspondantes et les chiffrer.

### Installation des outils tiers (Terraform, Helm, Python...) avec mise

Pour faciliter l'installation d'outils tiers (Terraform, Helm...) et le changement d'environnement de développement, nous utilisons [mise](https://mise.jdx.dev/). C'est un outil similaire à asdf.
Le guide d'installation est par [ici](https://mise.jdx.dev/getting-started.html). Une fois installé, il faudra naviguer dans le sous-répertoire où se situent les librairies de l'outil à utiliser (`terraform/` par exemple) puis lancer la commande `mise install`. L'outil se chargera alors de détecter lui-même les outils à installer et quelle version.
Vous pouvez également créer un fichier `.mise.toml` à la racine de ce sous-répertoire et indiquer précisément la version de l'outil à utiliser. Vous trouverez plus d'infos sur ce fichier [ici](https://mise.jdx.dev/configuration.html).

### ~/.ssh/config

Afin d'utiliser les bons identifiants et ports de connexion, il est nécessaire de définir les hôtes à piloter dans le fichier `~/.ssh/config`. 
Voici un exemple :

```bash
Host server_prd_01 server.prd01.lydra.eu 192.168.1.207
    HostName server.prd01.lydra.eu
    Port 1234
    User thomas
```

Sur la ligne `Host` on peut mettre autant d'alias que désiré, séparé par un espace, le serveur pourra être adressé ainsi avec la commande `ssh server_prd_01` et utiliser directement les bons paramètres.

### PATH

Nous utilisons souvent des _helper scripts_ qui se trouvent dans les répertoires `bin/`.

Il faut ajouter ce chemin suivant dans votre *PATH* :

```bash
export PATH="$PATH:./bin"
```

### Hook

Nous utilisons un hook pour vérifier les fichiers vault non chiffrés.
Vous devez créer un lien symbolique.

Dans le répertoire Git :

```bash
make install_hooks
```

### Rôles Ansible et Docker

Afin de disposer de toutes les ressources nécessaires (Docker, Ansible, paquets Python, rôles, etc), il est nécessaire de lancer la commande `make` à la racine du projet.

Cette commande permet des paramètres, si on souhaite ne rafraîchir qu'une partie :

```bash
make				                # contrôle les prérequis et installe l'environnement de dev
make install_deps		        # installe les paquets python, les rôles et collections Ansible
make install_pdm_deps		    # installe les paquets python
make install_ansible_deps		# installe les rôles et collections Ansible
make lint_rules             # Installe les règles de lint
make delete_deps		        # supprime les rôles, collections Ansible et dépendances PDM
make renew_deps		          # supprime puis réinstalle les dépendances
make refresh_ansible_deps		# force la mise à jour des rôles et collections
make install_hooks          # installe le hook de pré-commit
```

**Il est important de relancer la commande `make renew_deps` régulièrement pour charger en local les nouveaux rôles Ansible utilisés par le projet.**

### Les logs Ansible

[ARA](https://ara.recordsansible.org) est un outil web (local) qui facilite la lecture et l'exploration des logs produites par Ansible.

La configuration du `pyproject.toml` a été faite grâce à la commande :

```bash
pdm add ara[server]==1.7.0
```

Démarrer l'outil **avant** de lancer ses scripts Ansible :

```bash
export ANSIBLE_CALLBACK_PLUGINS="$(pdm run python -m ara.setup.callback_plugins)"
pdm run ara-manage runserver &
```

Attention, **lors du premier lancement dans un environnement vierge** , il se peut que vous soyez confrontés au message suivant : `"You have 28 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, api, auth, contenttypes, db, sessions.
Run 'python manage.py migrate' to apply them."`. Le tout accompagné d'une erreur 500 lorsque vous accédez au serveur de logs.
Dans ce cas, exécutez la commande suivante : `pdm run ara-manage migrate` pour régler ce souci.

Visualiser les logs : <http://127.0.0.1:8000>

## Utilisation

### Tester

Vérifier la configuration de votre environnement avec la commande de test :

```bash
provision test --tags=info
```

### Validité du code (Lint)

Pour tester la syntaxe de son code, on peut utiliser la commande `lint` qui fera divers contrôles et recommandations.

```bash
lint
```

Le contrôle sera fait par un `ansible-playbook --syntax-check` puis un `ansible-lint [...]` sur chacun des fichiers Yaml.

### Provisionner l'infrastructure

Il faut lancer cette commande à chaque fois que l'on souhaite appliquer des changements globaux d'infrastructure :

```bash
provision <env_type> [options]
```

Par défaut, l'environnement sera `dev`.
Dont les paramètres sont :

* env_type : environnement à provisionner
	* dev : développement
	* stg : staging
  * rec : recette
	* prd : production
* options :
	* `--limit=<name>` : ne cibler qu'un sous-groupe ou qu'une machine de l'environnement ciblé
        * debian
        * server_rec_01
    * `--tags=<tags>` : ne provisionner qu'une partie identifiée par un tag
        * deploy
        * common
    * `--skip-tags=<tags>` : permet d'exclure certaines parties du script
    * `--check` : réalise un test sans rien exécuter
    * `--syntax-check` : contrôle la syntaxe (voir l'alternative Lint)
    * `--verbose`

Exemples :

```bash
provision dev --limit=server_dev_01 --tags=common
provision dev --limit=server_dev_01 --skip-tags=common
```

À noter que les variables peuvent être surchargées dans l’ordre suivant ([cf. doc Ansible](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#how-we-merge))
* le groupe `all`
* les autres groupes, dans l’ordre alphabétique (cf. [autre doc Ansible](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#ansible-variable-precedence))
    * si les groupes sont hiérarchisés, cette hiérarchie est respectée
* le `host`

### Déployer l'application

Cette commande est un alias de `provision` avec un tag prédéfini.
On utilise cette commande pour déployer uniquement l'application sans recharger/réinstaller certains paquets système.

```bash
deploy [<env_type>] [options]
```

Par défaut, l'environnement sera `dev`.
Les options sont les mêmes que pour `provision`.
L'option `--tags=deploy` est ajoutée par défaut.

### Crowdsec

Le playbook `security.yml` se charge d'installer le service Crowdsec sur la machine. Il faut ensuite se connecter sur la console web de Crowdsec pour enregistrer l'instance puis manuellement se connecter en SSH sur la machine à enregistrer pour taper la commande donnée par la console web.

```bash
sudo cscli console enroll <token>
sudo systemctl restart crowdsec.service 
```

Pour en savoir plus sur Crowdsec, vous pourrez vous rendre sur [le tutoriel suivant](https://www.it-connect.fr/threat-intelligence-protegez-vous-des-adresses-ip-malveillantes-avec-crowdsec/).

## Résumé

```bash
# Démarrer l'environnement
export PATH="./bin:$PATH"
cd ansible
export ANSIBLE_CALLBACK_PLUGINS="$(pdm run python -m ara.setup.callback_plugins)"

pdm run ara-manage runserver &
# (...) modification de ses scripts Ansible
# Tester
lint
provision dev --limit=server_dev_01 --tags=common --syntax-check
provision dev --limit=server_dev_01 --tags=common --check
# Lancer
provision dev --limit=server_dev_01 --tags=common
# Visualiser les logs sur http://127.0.0.1:8000
```

On peut également lancer un environnement virtuel avec PDM, à la manière de `pipenv shell`.
Pour cela, il faut utiliser la commande `eval $(pdm venv activate venv_name)`. De plus amples informations sont disponibles [ici](https://pdm-project.org/latest/usage/venv/#activate-a-virtualenv).
